# SpectrogramAnalysis

Simple project created as part of Signal Analysis classes. Analysing spectrograms of audio signals.

The project consists of an implementation of a spectrogram and analysis of audio signals based on it. Spectrogram is a representation of the energy distribution of the signal in the time-frequency domain.
